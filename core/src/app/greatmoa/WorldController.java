package app.greatmoa;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

/**
 * Created by m0ncld on 2015-10-16.
 */
public class WorldController implements ApplicationListener{
    private boolean pause = false;
    public final static float MAX_HEIGHT = 10;

    private GameStage gameStage;
    @Override
    public void create() {
        gameStage = new GameStage(MAX_HEIGHT,30f,50f);
        gameStage.createActors();
        Gdx.input.setInputProcessor(gameStage);
    }

    @Override
    public void resize(int width, int height) {}


    float time = 0;
    @Override
    public void render() {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (!pause){
            time += Gdx.graphics.getDeltaTime();
            if (time > 1 / 60) {
                gameStage.act(time);
                time = 0;
            }
            gameStage.draw();
        }
    }

    @Override
    public void pause() {
        pause = true;
    }

    @Override
    public void resume() {
        pause = false;
    }

    @Override
    public void dispose() {
        gameStage.dispose();
    }
}

package app.greatmoa;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by m0ncld on 2015-10-16.
 */
public class GameActor extends Image {
    private Body body;

    /** Creates an uninitialized sprite. The sprite will need a texture region and bounds set before it can be drawn. */
    public GameActor() {
        super();
    }

    /** Creates a sprite with width, height, and texture region equal to the size of the texture. */
    public GameActor(Texture texture) {
        super(texture);
    }

    public GameActor(TextureAtlas.AtlasRegion region) {
        super(region);
    }

    public GameActor(Sprite sprite) {
        super(sprite);
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public float getOrigPosX(){
        return getX()+getOriginX();
    }

    public float getOrigPosY(){
        return getY()+getOriginY();
    }

    public void setOrigPosX(float x){
        setX(x-getOriginX());
    }

    public void setOrigPosY(float y){
        setY(y - getOriginY());
    }

    public void updatePos(){
        setOrigPosX(body.getPosition().x);
        setOrigPosY(body.getPosition().y);
    }
}

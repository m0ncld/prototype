package app.greatmoa;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by m0ncld on 2015-10-16.
 */
public class GameStage extends Stage {
    private World world;
    public final float SCALE;
    public final float MAX_HEIGHT;
    private Map<String, GameActor> actorRegister = new HashMap<String, GameActor>();
    private final float POWER;
    private final float MAX_POWER;
    public GameStage (float maxWorldHeightM, float power,float maxPower) {
        super();
        POWER = power;
        MAX_POWER = maxPower;
        MAX_HEIGHT = maxWorldHeightM;
        SCALE = countScale();
        float w = (float) Gdx.graphics.getWidth()/ SCALE;
        float h = (float) Gdx.graphics.getHeight()/ SCALE;
        setViewport(new FitViewport(w, h, new OrthographicCamera(w, h)));
        world = new World(new Vector2(0,-9.8f),true);
    }

    @Override
    public void dispose() {
        super.dispose();
        world.dispose();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        world.step(delta, 6, 2);
        for (Actor elem : getActors()){
            ((GameActor) elem).updatePos();
        }
    }

    @Override
    public void draw() {
        super.draw();
    }

    public void createActors(){
        addActor(createPlayer());
        addActor(createTemp());
        addActor(createGround());
    }

    private Actor createTemp() {
        Texture tx = new Texture("box.png");
        GameActor gameActor = new GameActor(tx);
        gameActor.setName("box");
        gameActor.setSize(tx.getWidth() / 20, tx.getHeight() / 20);
        gameActor.setOrigin(gameActor.getWidth() / 2, gameActor.getHeight() / 2);
        gameActor.setOrigPosX(0f);
        gameActor.setOrigPosY(0f);
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(gameActor.getOrigPosX(), gameActor.getOrigPosY());

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(gameActor.getWidth()/2, gameActor.getHeight()/2);
        gameActor.setBody(world.createBody(bodyDef));
        gameActor.getBody().createFixture(shape, 0.4f);
        gameActor.setTouchable(Touchable.enabled);
        shape.dispose();
        return gameActor;
    }

    private GameActor createPlayer(){
//        Sprite s = new Sprite();
//        s.setSize(1f, 1f);
        GameActor gameActor = new GameActor(new TextureAtlas(Gdx.files.internal("game.pack")).findRegion("dot"));
        gameActor.setName("dot");
        gameActor.setSize(1f, 1f);
        gameActor.setOrigin(gameActor.getWidth()/2, gameActor.getHeight()/2);
        gameActor.setOrigPosX(-0.75f);
        gameActor.setOrigPosY(10f);
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(gameActor.getOrigPosX(), gameActor.getOrigPosY());

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(gameActor.getWidth()/2, gameActor.getHeight()/2);
        gameActor.setBody(world.createBody(bodyDef));
        gameActor.getBody().createFixture(shape, 0.4f);
        gameActor.setTouchable(Touchable.enabled);
        shape.dispose();
        return gameActor;
    }

    private Actor createGround() {
//        Sprite s = new Sprite();
//        s.setSize(getCamera().viewportWidth,2);
        GameActor gameActor = new GameActor(new TextureAtlas(Gdx.files.internal("game.pack")).findRegion("ground"));
        gameActor.setName("ground");
        gameActor.setSize(getCamera().viewportWidth,2);
        gameActor.setOrigin(gameActor.getWidth()/2, gameActor.getHeight()/2);
        gameActor.setOrigPosX(0f);
        gameActor.setOrigPosY(-getCamera().viewportHeight/2);
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(gameActor.getOrigPosX(), gameActor.getOrigPosY());

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(gameActor.getWidth()/2, gameActor.getHeight()/2);
        gameActor.setBody(world.createBody(bodyDef));
        gameActor.getBody().createFixture(shape, 04f);
        gameActor.setTouchable(Touchable.disabled);
        shape.dispose();
        return gameActor;
    }

    private float countScale(){
        return Gdx.graphics.getHeight()/MAX_HEIGHT;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        super.touchUp(screenX, screenY, pointer, button);


        GameActor player = findActor("dot");
        Vector3 trans = getCamera().unproject(new Vector3(screenX, screenY, 0f));
        float s = MAX_POWER/(float)(Math.sqrt(Math.pow(trans.x,2)+Math.pow(trans.y,2)));
        if (s > 1f) s =1f;
        trans.x *= s;
        trans.y *= s;
        Vector2 force = new Vector2(POWER*(trans.x-player.getBody().getPosition().x),POWER*(trans.y-player.getBody().getPosition().y));
        player.getBody().applyForce(force,player.getBody().getPosition(),true);
        return true;
    }

    public GameActor findActor(String name){
        return actorRegister.get(name);
    }

    @Override
    public void addActor(Actor actor) {
        super.addActor(actor);
        actorRegister.put(actor.getName(), (GameActor) actor);
    }
}
